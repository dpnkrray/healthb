var moment = require('moment-timezone')

exports.getFormattedTime = function(t) {
  if (t !== '--') {
  //  var lt = moment.tz(t).format("YYYY-MM-DD HH:mm:ss")
    var dateUTC = new Date(t);
    var dateUTC = dateUTC.getTime()
    var dateIST = new Date(dateUTC);
    //date shifting for IST timezone (+5 hours and 30 minutes)
    dateIST.setHours(dateIST.getHours() + 5);
    dateIST.setMinutes(dateIST.getMinutes() + 30);
    return  moment(dateIST).format("HH:mm:ss")
  }
}
exports.getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

exports.round = function round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}

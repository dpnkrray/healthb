var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment')
/* GET ALL TRAINS */
router.get('/', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT * FROM logbook', function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      console.log(results);
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* GET ALL TRAINS */
router.get('/:id', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  //console.log(req.params.device_id)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT * FROM logbook where id= ?', [req.params.id], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* GET UP TRAINS AGAINST DN TRAIN*/
router.get('/doclistmaster/:id', passport.authenticate('jwt', { session: false}), function(req, res) {

  var token = getToken(req.headers);
  if (token) {
    pool.query(`SELECT * logbook where id = ?`,
      [req.params.id], function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
    } else {
      return res.status(403).send({
        success: false,
        msg: 'Unauthorized.'
      });
    }
  });

/* SAVE BOOK */
router.post('/', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
//  console.log('inside onr post ', req.body);
  if (!req.body.username || !req.body.user_type) {
    return res.json({
      success: false,
      msg: 'Please pass document name and document authority'
    });
  }
  pool.query('SELECT * FROM logbook WHERE username = ? and user_type= ?',
    [req.body.username, req.body.user_type],
    function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      if (typeof results !== 'undefined' && results.length > 0) {
        return res.status(401).send({
          success: false,
          msg: 'log already added.'
        });
      }
      var doclistmaster = {
        date_time:req.body.date_time,
        username:req.body.username,
        user_type:req.body.user_type,
        document_id:req.body.document_id,
        event_type:req.body.event_type
      }
      var query = pool.query('INSERT INTO logbook SET ?', doclistmaster, function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json({
          success: true,
          msg: 'Successfully added new employee.',
          results: results
        });
      });
    });
});

/* UPDATE logbook */
router.put('/', function(req, res, next) {
//  console.log('inside train put ', req.body);
  if (!req.body.username || !req.body.user_type) {
    return res.json({
      success: false,
      msg: 'Please pass document name and document authority'
    });
  }
  var doclistmaster = {
    date_time:req.body.date_time,
    username:req.body.username,
    user_type:req.body.user_type,
    document_id:req.body.document_id,
    event_type:req.body.event_type
  }

  pool.query('Update logbook SET ? WHERE id = ?', [doclistmaster, req.body.id],
    function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json({
        success: true,
        msg: 'Successfully updated data.',
        results: results
      });
    });
});

/* DELETE BOOK */
router.delete('/', function(req, res, next) {
//  console.log('inside logbook delete ', req.query);
  pool.query('delete from logbook WHERE id = ?',
    [req.query.id],
    function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json({
        success: true,
        msg: 'Successfully deleted employee.'
      });
    });
});

getToken = function(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;

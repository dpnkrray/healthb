var passport = require('passport');
var bcrypt = require('bcrypt');
var settings = require('../config/settings');
require('../config/passport')(passport);
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');

function cryptPassword(password, cb) {
  bcrypt.genSalt(10, function(err, salt) {
    if (err) {
      cb(err);
    }
    bcrypt.hash(password, salt, null, function(err, hash) {
      if (err) {
        cb(err);
      }
      password = hash;
      cb(null, password);
    });
  });
}

function comparePassword(passw, dbpassw, cb) {
  bcrypt.compare(passw, dbpassw, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    return cb(null, isMatch);
  });
}
router.get('/verifyjwt', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var token
  try{
     token = getToken(req.headers);
  } catch(err) {
    return res.status(403).send({
      success: false,
      message: 'unable to verify token'
    });
  }
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.status(403).send({
          success: false,
          message: 'Token is not valid'
        });
      } else {
        return res.json({
          username: decoded.username,
          user_type: decoded.user_type,
          loggedin: true,
          sup_id: decoded.sup_id,
          sup_name: decoded.sup_name
        });
      }
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

router.post('/register', function(req, res) {
  console.log('inside register ', req.body.username);
  if (!req.body.username || !req.body.password) {
    res.json({
      success: false,
      msg: 'Please pass username and password.'
    });
  } else {
    pool.query('SELECT * FROM users WHERE username = ?', [req.body.username], function(err, user, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      console.log(user);
      if (typeof user !== 'undefined' && user.length > 0) {
        return res.status(401).send({
          success: false,
          msg: 'User already exist.'
        });
      }
      bcrypt.genSalt(10, function(err, salt) {
        console.log('inside gesalt', salt);
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        console.log('inside gesalt', salt);
        bcrypt.hash(req.body.password, salt, function(err, hash) {
          console.log('inside hash', hash);
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          password = hash;
          var newUser = {
            username: req.body.username,
            password: password,
            user_type: req.body.user_type,
            sup_id: req.body.sup_id,
            sup_name: req.body.sup_name
          };
          console.log(newUser);
          var query = pool.query('INSERT INTO users SET ?', newUser, function(err, results, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }

            var query = pool.query(`INSERT INTO logbook (username,user_type,event_type)
               values  ('${req.body.username}','${req.body.user_type}','New User created')`, function(err, results3, fields) {
              if (err) {
                return res.status(401).send({
                  success: false,
                  msg: err
                });
              }
              return res.json({
                success: true,
                msg: 'Successful created new user.'
              });
            });
          });
        });
      });
    });
  }
});



router.post('/login', function(req, res) {
    pool.query('SELECT * FROM users WHERE username = ?', [req.body.username], function(err, results, fields) {
    // console.log(result)
    if (typeof results === 'undefined' || results.length == 0) {
      res.status(401).send({
        success: false,
        msg: 'Authentication failed. User not found.'
      });
    } else {
      var user = {};
      user = JSON.parse(JSON.stringify(results[0]));

      // check if password matches
      comparePassword(req.body.password, user.password, function(err, isMatch) {
        if (isMatch && !err) {
          // if user is found and password is right create a token
          var token = jwt.sign(user, settings.secret);
          // return the information including token as JSON
          var query = pool.query(`INSERT INTO logbook (username,user_type,event_type)
             values  ('${user.username}','${user.user_type}','Log in')`, function(err1, results3, fields) {
            if (err) {
              console.log('in success log err')
            return  res.status(401).send({
                success: false,
                msg: err1
              });
            }
            console.log('in success log success')
            res.json({
              success: true,
              token: 'JWT ' + token,
              user: {
                sup_name: user.sup_name,
                sup_id: user.sup_id,
                username: user.username,
                user_type: user.user_type,
                loggedin: true
              }
            });
          });
        } else {
          var query = pool.query(`INSERT INTO logbook (username,user_type,event_type)
             values  ('${user.username}','${user.user_type}','Log in Failed')`, function(err1, results3, fields) {
               console.log('in fail log')
              return res.status(401).send({
              success: false,
              msg: err1
            });
          });
        }
      });
    }
  });
});


module.exports = router;

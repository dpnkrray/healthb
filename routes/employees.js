var express = require('express');
var router = express.Router();
var passport = require('passport');
require('../config/passport')(passport);
var pool = require('../config/mysqlconnpool').pool;
var moment = require('moment');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var settings = require('../config/settings');
require('../config/passport')(passport);

/* GET ALL TRAINS */
router.get('/', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT (@sno := @sno + 1) as Sno,`employees`.`Name`,' +
    '`employees`.`Present Designation`,' +
    '`employees`.`Working as`,' +
    '`employees`.`Working at`,' +
    '`employees`.`Posting Shed`,' +
    '`employees`.`Gradation`,' +
    '`employees`.`Name of LI or SLI`,' +
    '`employees`.`Qualifications`,' +
    '`employees`.`Date of Appointment`,' +
    '`employees`.`Date of Birth`,' +
    '`employees`.`Date of joining in AC Traction`,' +
    '`employees`.`pf no`,' +
    '`employees`.`emp_id`,' +
    '`employees`.`Date of joining as Asst Driver`,' +
    '`employees`.`Date of Promotion as Sr Asst Driver`,' +
    '`employees`.`Date of Promotion AD HOC as ET`,' +
    '`employees`.`Date of Promotion Rgular as ET`,' +
    '`employees`.`Date of Promotion as Sr ET`,' +
    '`employees`.`Detail of Promotion as Sr ET`,' +
    '`employees`.`Date of Promotion AD HOC as Goods Driver`,' +
    '`employees`.`Date of Promotion Rgular as Goods Driver`,' +
    '`employees`.`Date of Promotion as Sr Goods Driver`,' +
    '`employees`.`Date of Promotion AD HOC as Pass Driver`,' +
    '`employees`.`Date of Promotion as Rgular  Pass Driver`,' +
    '`employees`.`Date of Promotion as Sr Pass Driver`,' +
    '`employees`.`Date of Promotion AD HOC as Spl Grade Driver`,' +
    '`employees`.`Date of Promotion Regular as Spl Grade Driver`,' +
    '`employees`.`Detail of Promotions`,' +
    '`employees`.`Date of attaining 45 years`,' +
    '`employees`.`Date of attaining 55 years`,' +
    '`employees`.`Date of Retirement`,' +
    '`employees`.`Last PME done on`,' +
    '`employees`.`PME due on`,' +
    '`employees`.`Detail of PMEs`,' +
    '`employees`.`With Spectacles`,' +
    '`employees`.`With NV Spectacles`,' +
    '`employees`.`Last ZTC done on`,' +
    '`employees`.`ZTC due on`,'+
    '`employees`.`Last Technical Ref done on`,'+
    '`employees`.`Technical Ref due on`,'+
    '`employees`.`Detail ofTechnical Refresher`,'+
    '`employees`.`Last Safety Camp or seminar attended on`,'+
    '`employees`.`Detail of Safety Camp or seminar`,'+
    '`employees`.`Present Address`,'+
    '`employees`.`Present City`,'+
    '`employees`.`Present District`,'+
    '`employees`.`Present P O`,'+
    '`employees`.`Present PIN`,'+
    '`employees`.`Present Phone No`,'+
    '`employees`.`Present State`,'+
    '`employees`.`Permanent Address`,'+
    '`employees`.`Permanent City`,'+
    '`employees`.`Permanent District`,'+
    '`employees`.`Permanent P O`,'+
    '`employees`.`Permanent PIN`,'+
    '`employees`.`Permanent Phone No`,'+
    '`employees`.`Permanent State`,'+
    '`employees`.`Punishment`,'+
    '`employees`.`Rewards & acheivements`,'+
    '`employees`.`Status`,'+
    '`employees`.`Other informations`,'+
    '`employees`.`Q_TYPE`,'+
    '`employees`.`AGE_GROUP`,'+
    '`employees`.`WORKING IN`,'+
    '`employees`.`PASS_SRL`,'+
    '`employees`.`DESIG_SRL`,'+
    '`employees`.`FULL NAME`,'+
    '`employees`.`Uniform Received`,'+
    '`employees`.`Automatic Cert Due on`,'+
    '`employees`.`seniority_pos`,'+
    '`employees`.`CUG_NO`,'+
    '`employees`.`BILL_UNIT`,'+
    '`employees`.`CATEGORY`,'+
    '`employees`.`PSY_ATTND`,'+
    '`employees`.`PSY_DATE`,'+
    '`employees`.`PSY_RESULT`,'+
    '`employees`.`PSY_DETAIL`,'+
    '`employees`.`no_of_app_inst`,'+
    '`supervisors`.`Name` as CLI,' +
    '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,'+
    "`employees`.`Detail of ZTC` FROM `employees` left join `supervisors` on `employees`.`Name of LI or SLI` = `supervisors`.`emp_id` left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where status = 'WORKING' and `employees`.`Present Designation` not in ('CLI') order by `Designation`,`Posting Shed`,Name", function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});



/* GET ALL TRAINS */
router.get('/emp_id', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  //console.log(req.params.device_id)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT * FROM employees where emp_id= ?', [req.query.id], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* Get loco info from loco table*/
router.get('/by_shed', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `employees`.`Name`,' +
    '`employees`.`Present Designation`,' +
    '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,'+
    '`employees`.`Posting Shed`,' +
    '`employees`.`Gradation`,' +
    '`employees`.`pf no`,' +
    '`employees`.`CUG_NO`,'+
    '`employees`.`Name of LI or SLI`,' +
    '`employees`.`emp_id`,'+
    '`employees`.`Status`,'+
    '`supervisors`.`Name` as CLI ' +
    "FROM employees  left join `supervisors` on `employees`.`Name of LI or SLI` = `supervisors`.`emp_id` left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where status = 'WORKING' and `employees`.`Posting Shed` = ? order by name",
     [req.query.shed], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});


/* Get loco info from loco table*/
router.get('/sup_id', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT (@sno := @sno + 1) as Sno,`employees`.`Name`,' +
    '`employees`.`Present Designation`,' +
    '`desig_srl`.`C_NEW_DESIG_SHORTEST` as Designation,'+
    '`employees`.`Posting Shed`,' +
    '`employees`.`Gradation`,' +
    '`employees`.`pf no`,' +
    '`employees`.`CUG_NO`,'+
    '`employees`.`Name of LI or SLI`,' +
    '`employees`.`emp_id`,'+
    '`employees`.`Status`,'+
    "health_status.health_status,health_status.health_description,health_status.no_of_app_inst FROM employees left join health_status on `employees`.`emp_id` = health_status.emp_id and health_status.status_date= ?  left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG, (SELECT @sno := 0) as ini where `Name of LI or SLI`= ? and status = 'WORKING' order by `Designation`,`Posting Shed`,Name",
     [req.query.status_date, req.query.sup_id], function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* Get loco info from loco table*/
router.get('/cli_dist', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT  (@sno := @sno + 1) as Sno,`supervisors`.`Name`,`supervisors`.`emp_id`,'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")  THEN 1 ELSE 0 END) AS "LPM",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MOTORMAN")  THEN 1 ELSE 0 END) AS "MOTORMAN",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("TW Driver")  THEN 1 ELSE 0 END) AS "TW Driver",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN'+
   '("MAIL DRIVER","PASS DRIVER","SR. PASS DRIVER","GOODS DRIVER","SR. GOODS DRIVER","ET","SR. ET","ASST. DRIVER","SR. ASST. DRIVER","TW Driver","MOTORMAN")'+
    ' THEN 0 ELSE 1 END) AS "Other",'+
   'SUM( 1 ) AS "TOTAL",'+
   'SUM(CASE WHEN `employees`.`health_status` is null THEN 0 ELSE 1 END) AS "Health Status",' +
   'SUM(`employees`.`no_of_app_inst`) AS "No of App" ' +
    'FROM '+
    '  employees left join supervisors on `employees`.`Name of LI or SLI` = supervisors.emp_id, (SELECT @sno := 0) as ini'+
    " WHERE STATUS = 'WORKING'  and `employees`.`Present Designation` not in ('CLI','ACC(R)','CC(R)')"+
    ' GROUP BY `supervisors`.`Name`,`supervisors`.`emp_id` order by `supervisors`.`Name`', function(err, results, fields) {
    if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* Get loco info from loco table*/
router.get('/staff_position', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `employees`.`Posting Shed`,'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")  THEN 1 ELSE 0 END) AS "LPM",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MOTORMAN")  THEN 1 ELSE 0 END) AS "MOTORMAN",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("CC(R)","ACC(R)","PC") THEN 1 ELSE 0 END) AS "Supv",'+
   'SUM( 1 ) AS "TOTAL" '+
    'FROM '+
    "employees left join desig_srl on `employees`.`Present Designation` = desig_srl.DESIG WHERE STATUS = 'WORKING' and `employees`.`Present Designation` not in ('CLI','TW Driver') "+
    'GROUP BY `employees`.`Posting Shed`', function(err, results, fields) {
    if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* Get loco info from loco table*/
router.get('/sup_health_status', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `supervisors`.`Name`,`supervisors`.`emp_id`,'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("MAIL DRIVER")  THEN 1 ELSE 0 END) AS "LPM",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("PASS DRIVER","SR. PASS DRIVER")  THEN 1 ELSE 0 END) AS "LPP",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("GOODS DRIVER","SR. GOODS DRIVER")  THEN 1 ELSE 0 END) AS "LPG",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ET","SR. ET")  THEN 1 ELSE 0 END) AS "LPS",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("ASST. DRIVER","SR. ASST. DRIVER")  THEN 1 ELSE 0 END) AS "ALP",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN ("TW Driver")  THEN 1 ELSE 0 END) AS "TW Driver",'+
   'SUM(CASE WHEN  `employees`.`Present Designation` IN'+
   '("MAIL DRIVER","PASS DRIVER","SR. PASS DRIVER","GOODS DRIVER","SR. GOODS DRIVER","ET","SR. ET","ASST. DRIVER","SR. ASST. DRIVER","TW Driver")'+
    ' THEN 0 ELSE 1 END) AS "Other",'+
   'SUM( 1 ) AS "TOTAL",'+
   'SUM(CASE WHEN `health_status`.`health_status` is null THEN 0 ELSE 1 END) AS "Health Status",' +
   'SUM(`health_status`.`no_of_app_inst`) AS "No of App" ' +
    'FROM '+
    '  employees left join supervisors on `employees`.`Name of LI or SLI` = supervisors.emp_id left join health_status on `employees`.`emp_id` = health_status.emp_id and  health_status.status_date= ?'+
    " WHERE STATUS = 'WORKING' "+
    ' GROUP BY `supervisors`.`Name`,`supervisors`.`emp_id`',[req.query.status_date],function(err, results, fields) {
    if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});



/* Get loco info from loco table*/
router.get('/health_status', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req)
  var token = getToken(req.headers);
  if (token) {
    pool.query("SELECT max(`health_status`.`status_date`) as 'status_date',"+
   "SUM(CASE WHEN  employees.status='WORKING' and `health_status`.`health_status` IN ('ok')  THEN 1 ELSE 0 END) AS 'ok',"+
   "SUM(CASE WHEN  employees.status='WORKING' and `health_status`.`health_status` IN ('not_ok')  THEN 1 ELSE 0 END) AS 'not_ok',"+
   "SUM(CASE WHEN  employees.status='WORKING' and `health_status`.`health_status` IS NULL  THEN 1 ELSE 0 END) AS 'not_reported',"+
   "SUM(CASE WHEN  employees.status='WORKING' THEN `health_status`.`no_of_app_inst` ELSE 0 END) AS 'no_of_app_inst',"+
   "SUM(case when employees.status='WORKING' then 1 else 0 end ) AS 'total' "+
    'FROM '+
    "employees left join health_status on employees.emp_id = health_status.emp_id and employees.status='WORKING' and status_date = ?" ,[req.query.status_date],function(err, results, fields) {
    if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

/* GET ALL TRAINS */
router.get('/not_ok_not_reported', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    pool.query('SELECT `employees`.`Name`,' +
    '`employees`.`Present Designation`,' +
    '`employees`.`Posting Shed`,' +
    '`employees`.`pf no`,`employees`.`CUG_NO`,`supervisors`.`Name` as CLI, health_status.health_status, health_status.health_description,' +
    '`employees`.`emp_id`,' +
    '`employees`.`Status` '+
     "FROM`employees` left join `supervisors` on `employees`.`Name of LI or SLI` = `supervisors`.`emp_id` left join health_status on `employees`.`emp_id` = health_status.emp_id where (health_status.health_status ='not_ok' and status_date = ?) or (status = 'WORKING' and health_status.health_status is null) order by employees.Name" ,[req.query.status_date] ,function(err, results, fields) {
      if (err) {
        return res.status(401).send({
          success: false,
          msg: err
        });
      }
      return res.json(results);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});


/* GET UP TRAINS AGAINST DN TRAIN*/
router.get('/employee/:employee_id', passport.authenticate('jwt', {
  session: false
}), function(req, res) {

  var token = getToken(req.headers);
  if (token) {
    pool.query(`SELECT * employees where employee_id = ?`,
      [req.params.employee_id],
      function(err, results, fields) {
        if (err) {
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        return res.json(results);
      });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});



/* Update healh_status */
router.put('/update_cli', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req.body)
  // console.log('inside onr post ', req.body);
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      }

      // if (!req.body.cli_id) {
      //   return res.json({
      //     success: false,
      //     msg: 'Please assign CLI'
      //   });
      // }
      var emp = {
        CUG_NO: req.body.CUG_NO,
        'Name of LI or SLI': req.body.cli_id,
        'Present Designation': req.body['Present Designation'],
        'Posting Shed': req.body['Posting Shed'],
         Status: req.body.Status
      }
      pool.query('Update employees SET ? WHERE emp_id = ?', [emp, req.body.emp_id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          return res.json({
            success: true,
            msg: 'Successfully updated CLI.',
          });
        });
    });
  } else {
    return res.status(401).send({
      success: false,
      msg: 'unautorised'
    });
  }
});
/* Update healh_status */
router.put('/update_health', passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  // console.log(req.body)
  // console.log('inside onr post ', req.body);
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.status(401).send({
          success: false,
          message: 'Token is not valid'
        });
      }

      if (!req.body.CUG_NO) {
        return res.status(401).send({
          success: false,
          msg: 'Please pass phone no.'
        });
      }
      pool.getConnection(function(err, connection) {
        if (err) {
          if (connection) {
            connection.release()
          }
          return res.status(401).send({
            success: false,
            msg: err
          });
        }
        var emp = {
          CUG_NO: req.body.CUG_NO,
          health_status: req.body.health_status,
          health_description: req.body.health_description,
          no_of_app_inst: req.body.no_of_app_inst
        }
        console.log(req.body.emp_id)
        connection.beginTransaction(function(err) {
          var query = connection.query('Update employees SET ? WHERE emp_id = ?', [emp, req.body.emp_id], function(err, results1, fields) {
          //  console.log(results1, err, fields)
            // if (err) {
            //   connection.rollback(function() {
            //     if (pool._freeConnections.indexOf(connection) {
            //       connection.release()
            //     }
            //     return res.status(401).send({
            //       success: false,
            //       msg: err
            //     });
            //   });
            // }

            var query = connection.query(`INSERT INTO logbook (username,user_type,document_id,event_type)
           values  ('${decoded.username}','${decoded.user_type}',${req.body.emp_id},'Update Health Status')`, function(err, results2, fields) {
              // if (err) {
              //   connection.rollback(function() {
              //     if (pool._freeConnections.indexOf(connection) {
              //       connection.release()
              //     }
              //     return res.status(401).send({
              //       success: false,
              //       msg: err
              //     });
              //   });
              // }
              var query = connection.query(`INSERT INTO health_status (status_date,cli_id,emp_id,health_status,health_description,created_by,no_of_app_inst)
                 VALUES ('${req.body.status_date}',${req.body.cli_id},${req.body.emp_id},'${req.body.health_status}','${req.body.health_description}',
                    '${decoded.username}',${req.body.no_of_app_inst}) ON DUPLICATE KEY UPDATE health_status='${req.body.health_status}',
                     health_description='${req.body.health_description}',no_of_app_inst=${req.body.no_of_app_inst},
                     updated_by = '${decoded.username}'`,
                function(err, results3, fields) {
                  if (err) {
                    connection.rollback(function() {
                      if (pool._freeConnections.indexOf(connection) < 0) {
                        connection.release()
                      }
                      return res.status(401).send({
                        success: false,
                        msg: err
                      });
                    });
                  } else {
                    connection.commit(function(err) {
                      if (err) {
                        connection.rollback(function() {
                          if (pool._freeConnections.indexOf(connection) < 0) {
                            connection.release()
                          }
                          return res.status(401).send({
                            success: false,
                            msg: err
                          });
                        });
                      }
                      if (pool._freeConnections.indexOf(connection) < 0) {
                        connection.release()
                      }
                      return res.json({
                        success: true,
                        msg: 'Successfully update health status.',
                      });
                    });
                  }
                });
            });
          });
        });
      });
    });
  } else {
    return res.status(401).send({
      success: false,
      msg: 'unautorised'
    });
  }
});






/* UPDATE employees */
router.put('/', passport.authenticate('jwt', {
  //  console.log('inside train put ', req.body);
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {


        if (!req.body.loco_no) {
          return res.json({
            success: false,
            msg: 'Please pass loco No'
          });
        }
      }


      var onrcase = {
        Name: req.body.Name,
        Present_Designation: req.body.Present_Designation,
        Working_as: req.body.Working_as,
        Working_at: req.body.Working_at,
        Posting_Shed: req.body.Working_at,
        Gradation: req.body.Gradation,
        Name_of_LI_or_SLI: req.body.Name_of_LI_or_SLI,
        Qualifications: req.body.Qualifications,
        Date_of_Appointment: req.body.Date_of_Appointment,
        Date_of_Birth: req.body.Date_of_Birth,
        Date_of_joining_in_AC_Traction: req.body.Date_of_joining_in_AC_Traction,
        pf_no: req.body.pf_no,
        emp_id: req.body.emp_id,
        Date_of_Retirement: req.Date_of_Retirement,
        Present_Phone_No: req.body.Present_Phone_No,
        cUG_NO: req.body.cUG_NO,
        healh_status: req.healh_status,
        healh_description: req.body.healh_description,
      }

      pool.query('Update employees SET ? WHERE id = ?', [onrcase, req.body.id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          // var query = pool.query('INSERT INTO employees SET ?', onrcase, function(err, results, fields) {
          //   if (err) {
          //     return res.status(401).send({
          //       success: false,
          //       msg: err
          //     });
          //   }
          var query = pool.query(`INSERT INTO logbook (username,user_type,document_id,event_type)
         values  ('${decoded.username}','${decoded.user_type}',${req.body.id},'Edit')`, function(err, results2, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }

            return res.json({
              success: true,
              msg: 'Successfully updated Loco and log',
              results: results2
            });
          });
        })
    });
  }
});







/* DELETE BOOK */
router.delete('/', passport.authenticate('jwt', {
  //  console.log('inside train put ', req.body);
  session: false
}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    jwt.verify(token, settings.secret, (err, decoded) => {
      // if (err) {
      //   return res.json({
      //     success: false,
      //     message: 'Token is not valid'
      //   });
      // } else {
      //
      //
      //   if (!req.body.loco_no) {
      //     return res.json({
      //       success: false,
      //       msg: 'Please pass loco No'
      //     });
      //   }
      // }

      //  console.log('inside employees delete ', req.query);
      pool.query('delete from employees WHERE id = ?',
        [req.query.id],
        function(err, results, fields) {
          if (err) {
            return res.status(401).send({
              success: false,
              msg: err
            });
          }
          var query = pool.query(`INSERT INTO logbook (username,user_type,document_id,event_type)
         values  ('${decoded.username}','${decoded.user_type}','${req.query.id}','Delete')`, function(err, results3, fields) {
            if (err) {
              return res.status(401).send({
                success: false,
                msg: err
              });
            }



            return res.json({
              success: true,
              msg: 'Successfully deleted Train No.'

            });
          });
        });
    });
  }
});


let now = moment();
// console.log(now.format());

const crypto = require('crypto');
var datetime = require('node-datetime');
const multer = require('multer');
// const db = require('db');
const fs = require('fs');
// const uuidv1 = require('uuid/v1');
const path = require('path');

const shell = require('shelljs');



var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log(req.body)
    // initial upload path
    let destination = path.join('./public/files/'); // ./uploads/
    shell.mkdir('-p', './public/files/' + req.body.id);
    destination = path.join(destination, '', req.body.id); // ./uploads/files/generated-uuid-here/
    console.log('dest', destination)

    cb(
      null,
      destination
    );
  },
  filename: function(req, file, cb) {
    cb(null, moment().format('YYYY MM DD HH mm ss') + file.originalname)
  }

})

var upload = multer({
  storage: storage
})
// module.exports = (router) => {
//
//   router.get('/get', (req, res) => {
//     db.connect.query('SELECT * FROM employees',
//       {type: db.sequelize.QueryTypes.SELECT}
//     ).then(result => {
//       res.json(result);
//     })
//   });
//
//   router.post('/upload', upload.any(), (req, res) => {
//     res.json('test');
//
//
//   });
//
//   return router;
// };


router.post('/upload', upload.single('loco_trs'), (req, res, next) => {
  const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
  res.send(file)



})

// router.post('/upload', passport.authenticate('jwt', {
//   session: false
// }), function(req, res) {
//   var token = getToken(req.headers);
//   if (token) {
//     if (!req.files || Object.keys(req.files).length === 0) {
//       return res.status(400).send('No files were uploaded.');
//     }
//
//     // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
//     let sampleFile = req.files[0];
//
//     // Use the mv() method to place the file somewhere on your server
//     sampleFile.mv('/images/filename.jpg', function(err) {
//       if (err)
//         return res.status(500).send(err);
//
//       res.send('File uploaded!');
//     });
//     console.log(req.file)
//   }
//
// });
getToken = function(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;

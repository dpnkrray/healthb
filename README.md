# health
mysql -u root -p traccar_db < desig.sql
ALTER TABLE `traccar_db`.`health_status`
CHANGE COLUMN `health_status` `health_status` TEXT NULL DEFAULT NULL ;

ALTER TABLE `traccar_db`.`desig_srl`
ADD COLUMN `C_NEW_DESIG_SHORTEST` VARCHAR(45) NULL AFTER `NEW_DESIG_SHORTEST`,
ADD COLUMN `C_NEW_DESIG_SHORT` VARCHAR(45) NULL AFTER `C_NEW_DESIG_SHORTEST`;

INSERT INTO `traccar_db`.`desig_srl` (`SRL_NO`, `SRL_NO1`, `DESIG`, `C_DESIG`, `C_DESIG_NEW`, `DESIG_SRL`, `NEW_DESIG`, `NEW_DESIG_SHORT`, `NEW_DESIG_SHORTEST`, `C_NEW_DESIG_SHORTEST`, `C_NEW_DESIG_SHORT`) VALUES ('2', '2', 'MOTORMAN', 'MOTORMAN', 'MOTORMAN', '2', 'MOTORMAN', 'MOTORMAN', 'MOTORMAN', 'MOTORMAN', 'MOTORMAN');

UPDATE `traccar_db`.`employees` set `Present Designation` = 'MOTORMAN' WHERE `Present Designation` IN ('PASS DRIVER','SR. PASS DRIVER');

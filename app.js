var express = require('express');
var exp = require('express');

const fs = require('fs');
// const uuidv1 = require('uuid/v1');


const shell = require('shelljs');

var serveIndex = require('serve-index');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var cors = require('cors');
var supervisors = require('./routes/supervisors');
var auth = require('./routes/auth');
// var trsinspection = require('./routes/trsinspection');
var employees = require('./routes/employees');
var logbook = require('./routes/logbook');
// var benifitmaster = require('./routes/benifitmaster');
var moment = require('moment');
var moment = require('moment');
var _ = require('lodash')
var pool = require('./config/mysqlconnpool').pool;
var app = express();
var server = require('http').Server(app);
var webSocketServer = require('websocket').server;
const fileUpload = require('express-fileupload');
var CronJob = require('cron').CronJob;
// var trainsreport = require('./modules/trainsreport');
// var lateruntrains = require('./modules/lateruntrains');

// trainsreport.getDepArrTable('2019/10/17 00:00:00','2019/10/17 23:59:00', function(data) {
//    console.log(data)
// })
// lateruntrains.getLateRunTrains('2019/10/17 00:00:00',0, function(data) {
//    console.log(data)
// })
//app.use(express.static('public'));
app.use('/ftp', express.static('public'), serveIndex('public', {'icons': true}));
app.use(cors());
app.options('*', cors()) // include before other routes

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));

app.use('/auth', auth);
app.use('/employees', employees);
app.use('/supervisors', supervisors);
app.use('/logbook', logbook);

// const prepareCtrJob = new CronJob('0 0 2 * * *', function() {
//     pool.query('update employees set health_status = null,health_description=null',
//       function(err, results, fields) {
//         if (err) {
//           console.log('err in emploees table health data reset',err)
//         }
//         console.log('emploees table health data reset at ', moment().tz('Asia/Kolkata').format('DD/MM/YY HH:mm:ss'))
//       })
// }, null, true, 'Asia/Kolkata', null, true);
//
// prepareCtrJob.start()


//catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// restful api error handler
app.use(function(err, req, res, next) {
  console.log(err);

  // if (req.app.get('env') !== 'development') {
  //     delete err.stack;
  // }

	res.status(err.statusCode || 500).json(err);
});

module.exports = {app: app, server: server};
